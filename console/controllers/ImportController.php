<?php

namespace console\controllers;

use common\models\Agent;
use common\models\Photo;
use common\models\Property;
use common\models\PropertyHasAgent;
use jc21\CliTableManipulator;
use Yii;
use console\components\Controller;
use yii\helpers\Console;
use jc21\FileList;
use jc21\CliTable;
use common\models\xml\RenetProperties;


class ImportController extends Controller
{

    /**
     * @var string
     */
    public $xml = null;


    /**
     * @return array
     */
    public function options($actionID)
    {
        return [
            'xml',
        ];
    }


    /**
     * import
     *
     * @access public
     * @return int
     * @throws \Exception
     */
    public function actionIndex()
    {
        $files = $this->getFiles();

        foreach ($files as $file) {
            if ($this->xml && $this->xml !== $file[FileList::KEY_NAME]) {
                continue;
            }

            $xmlString  = file_get_contents($file['fullpath']);

            $this->printLine($file['fullpath'], Console::FG_YELLOW, true);

            if ($file[FileList::KEY_SIZE] == 0) {
                // Delete the file
                //unlink($file['fullpath']);
                //$this->printLine('  - Empty file deleted', Console::FG_RED, true);
                $this->printLine('  - File is empty (size 0)', Console::FG_RED, true);
                rename($file['fullpath'], $file['fullpath'] . '.empty');
                continue;
            }

            try {
                $properties = new RenetProperties($xmlString);

                if ($properties->isLoaded) {
                    $data = [];
                    if (isset($properties->residentials)) {
                        foreach ($properties->residentials as $property) {
                            /* @var $property \common\models\xml\Property */
                            $data[] = [
                                'address' => $property->getAddress(),
                                'authority' => $property->getAuthority(),
                                'underOffer' => $property->getUnderOffer(),
                                'category' => $property->getCategory(),
                                'auctionDate' => $property->getAuctionDate(),
                                'priceView' => $property->get('priceView'),
                                'stats' => implode('-', [
                                    $property->getBedrooms(),
                                    $property->getBathrooms(),
                                    $property->getGarages(),
                                ]),
                                'sold' => $property->isSold(),
                                'landArea' => $property->getLandArea(),
                                'soldDate' => $property->getSoldDate(),
                                'inspectionTimes' => ($property->getInspectionTimes() ? implode(', ', $property->getInspectionTimes()) : ''),
                                'type' => $property->getType(),
                            ];
                            print_r($property->getImages());
                        }


                        if (count($data)) {
                            $table = new CliTable('Property');
                            $table->setTableColor('blue');
                            $table->setHeaderColor('yellow');
                            $table->injectData($data);
                            $table->addField('Type', 'type', false, 'red');
                            $table->addField('Address', 'address', false, 'cyan');
                            $table->addField('authority', 'authority');
                            $table->addField('underOffer', 'underOffer', new CliTableManipulator('yesno'));
                            $table->addField('category', 'category');
                            $table->addField('auctionDate', 'auctionDate', new CliTableManipulator('datetime'));
                            $table->addField('price', 'priceView');
                            $table->addField('stats', 'stats');
                            $table->addField('sold', 'sold', new CliTableManipulator('yesno'));
                            $table->addField('landArea', 'landArea');
                            $table->addField('soldDate', 'soldDate', new CliTableManipulator('date'));
                            $table->addField('inspectionTimes', 'inspectionTimes');
                            $table->display();
                        } else {
                            $this->printLine('  - No properties found', Console::FG_RED, true);
                        }
                    } else {
                        $this->printLine('  - No residentials found in file', Console::FG_RED, true);
                    }

                } else {
                    Yii::error([
                        'msg' => 'Failed to load XML file',
                        'data' => ['filename' => $file['fullpath']],
                    ]);

                    $this->printLine('  - Failed to load', Console::FG_RED, true);
                }
            } catch (\Exception $e) {
                $this->printLine('  - ' . $e->getMessage(), Console::FG_RED, true);
                exit(1);
            }
        }
    }


    /**
     * import/data
     *
     * @access public
     * @return int
     * @throws \Exception
     */
    public function actionData()
    {
        $files = $this->getFiles();

        foreach ($files as $file) {
            if ($this->xml && $this->xml !== $file[FileList::KEY_NAME]) {
                continue;
            }

            $xmlString = file_get_contents($file['fullpath']);
            $this->printLine($file['fullpath']);

            if ($file[FileList::KEY_SIZE] == 0) {
                // Delete the file
                unlink($file['fullpath']);
                $this->printLine('- Empty file deleted');
                continue;
            }

            try {
                $properties = new RenetProperties($xmlString);

                if ($properties->isLoaded) {
                    if (isset($properties->residentials)) {
                        foreach ($properties->residentials as $property) {
                            /* @var $property \common\models\xml\Property */

                            if (!$property->getAddress()) {
                                continue;
                            }

                            $row = Property::findOne(['renetId' => $property->getUniqueId()]);
                            if (!$row) {
                                $row = new Property;
                                $row->renetId = $property->getUniqueId();
                            }

                            $row->type            = $property->getType();
                            $row->address         = $property->getAddress(true, true);
                            $row->authority       = $property->getAuthority();
                            $row->isUnderOffer    = $property->getUnderOffer();
                            $row->category        = $property->getCategory();
                            $row->auctionTime     = $property->getAuctionDate();
                            $row->displayPrice    = $property->getPriceView();
                            $row->bedrooms        = $property->getBedrooms();
                            $row->bathrooms       = $property->getBathrooms();
                            $row->garages         = $property->getGarages();
                            $row->isSold          = $property->isSold();
                            $row->landArea        = $property->getLandArea();
                            $row->soldTime        = $property->getSoldDate();
                            $row->inspectionTimes = $property->getInspectionTimes();
                            $row->headline        = $property->getHeadline();
                            $row->description     = $property->getDescription();
                            $row->listingAgents   = $property->getListingAgents();

                            if ($row->save()) {
                                $this->printLine('- Property ID #' . $row->id . ' Saved');
                                $this->printLine('  - Address: ' . $property->getAddress());
                                $this->printLine('  - Type: ' . $row->type);
                                $this->printLine('  - Renet ID: ' . $row->renetId);


                                // Photos
                                Photo::deleteAll(['propertyId' => $row->id]);

                                $order = 0;
                                foreach ($property->getImages() as $image) {
                                    $photo = new Photo;
                                    $photo->propertyId = $row->id;
                                    $photo->order = $order;
                                    $photo->url = $image;
                                    $photo->save();

                                    $order++;
                                }

                                $this->printLine('  - ' . $order . ' Photos saved');


                                // Agents
                                PropertyHasAgent::deleteAll(['propertyId' => $row->id]);
                                $listingAgents = $property->getListingAgents();

                                if ($listingAgents && is_array($listingAgents) && count($listingAgents)) {
                                    foreach ($listingAgents as $listingAgent) {
                                        if (isset($listingAgent['email'])) {
                                            $agent = Agent::findOne(['email' => strtolower($listingAgent['email'])]);
                                            if (!$agent) {
                                                $agent = new Agent;
                                                $agent->email = strtolower($listingAgent['email']);
                                            }

                                            $agent->name = $listingAgent['name'];
                                            $agent->save();

                                            // Agent has property
                                            $agent->link('properties', $row);
                                        }
                                    }
                                }

                                $this->printLine('  - Agent associations saved');

                            } else {
                                $this->printLine('- Property ID #' . $row->id . ' NOT Saved');
                            }
                        }

                    } else {
                        $this->printLine('- No properties found in file');
                    }

                    rename($file['fullpath'], $file['fullpath'] . '.imported');

                } else {
                    $this->printLine('- Failed to load XML file');
                    rename($file['fullpath'], $file['fullpath'] . '.failed');
                }
            } catch (\Exception $e) {
                $this->printLine('- ' . $e->getMessage());
                rename($file['fullpath'], $file['fullpath'] . '.failed');
            }
        }
    }


    /**
     * import/list
     *
     * @access public
     * @return int
     * @throws \Exception
     */
    public function actionList()
    {
        $files = $this->getFiles();

        foreach ($files as $file) {
            $this->printLine('- ' . $file[FileList::KEY_NAME] . ' (' . $file[FileList::KEY_SIZE] . ')', Console::FG_YELLOW, true);
        }
    }


    /**
     * @return array
     * @throws \Exception
     */
    protected function getFiles()
    {
        $fileList = new FileList;
        $folder   = Yii::$app->params['renetImportFolder'];
        $files    = $fileList->get($folder, FileList::TYPE_FILE, FileList::KEY_NAME, FileList::ASC, null, ['xml']);

        foreach ($files as &$file) {
            $file['fullpath'] = $folder . '/' . $file[FileList::KEY_NAME];
        }

        return $files;
    }
}
