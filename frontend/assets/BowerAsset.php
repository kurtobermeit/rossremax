<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class BowerAsset extends AssetBundle
{
    public $sourcePath = '@bower';

    public $css = [
        'bootstrap-select/dist/css/bootstrap-select.min.css',
    ];

    public $js = [
        'bootstrap/dist/js/bootstrap.min.js',
        'bootstrap-select/dist/js/bootstrap-select.js',
        'respond/dest/respond.min.js',
        'jquery_lazyload/jquery.lazyload.js'
    ];

    public $depends = [
        'yii\bootstrap\BootstrapAsset',
    ];
}