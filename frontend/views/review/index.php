<?php
/* @var $this yii\web\View */
/* @var $members common\models\Agent[] */

use yii\helpers\Html;
use common\components\Util;

$this->title = '';

?>


<section class="wrapper-sm">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?= $agent->name ?>'s Reviews</h1>
                <p class="lead">See what our clients are saying.</p>
            </div>
        </div>
    </div>
</section>

<section class="wrapper-md bg-highlight">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-3 col-md-offset-1">
                <div class="thumbnail">
                    <div class="overlay-container">
                        <img src="/images/agent_<?= strtolower(str_replace(' ', '', $agent['name'])); ?>_profile.jpg">
                    </div>
                    <div class="thumbnail-meta">
                        <i class="fa fa-fw fa-briefcase"></i> Licensed Agent
                    </div>
                    <div class="thumbnail-meta">
                        <i class="fa fa-fw fa-envelope"></i> <a
                            href="mailto:<?= Html::encode($agent->email) ?>"><?= Html::encode($agent->email) ?></a>
                    </div>
                    <div class="thumbnail-meta">
                        <i class="fa fa-fw fa-phone"></i> <?= Html::encode($agent->phone) ?>
                    </div>
                    <div class="thumbnail-meta">
                        <a href="#link" class="btn btn-lg btn-block btn-success">Contact</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-7 reviews">
                <?php if ($agent->id === 2) { // ROSS ?>
                    <h4>Paul & Jo O <span class="normal-weight">(Vendor)</span></h4>
                    <h5>Eight Mile Plains</h5>
                    <p>We found Ross & his team outstanding. Ross was knowledgeable, efficient & helped us achieve our desired result in selling our property in a rather short time frame. As my husband & I are busy working parents Ross & Emma were amazing in working around our time schedules & they even completed a few finishing touches to our home if we had run out of time. We had complete trust in them in securing our property after inspections if we were not at home. We definitely recommend Ross Bischoff & Team.
                    </p>

                    <h4>Carole G <span class="normal-weight">(Vendor)</span></h4>
                    <h5>Eight Mile Plains</h5>
                    <p>I would be happy to recommend Ross Bischoff and Emma De Marco of RE/MAX. Ross advised me to sell by auction and I received a good outcome. Ross and Emma made the whole process straight forward and I was kept fully informed of their progress.
                    </p>

                    <h4>Jerry X <span class="normal-weight">(Vendor)</span></h4>
                    <h5>Mansfield</h5>
                    <p>Ross helped us to sell our home in a difficult market (during Christmas). He helped us through the stressful time and provided us with sufficient information for us to achieve great results.
                    </p>

                    <h4>Sharon B & Sam S <span class="normal-weight">(Vendor)</span></h4>
                    <h5>Drewvale</h5>
                    <p>A fantastic experience! We wouldn't ever use anyone else to sell our house. Ross was awesome!
                    </p>

                    <h4>Marguerite C <span class="normal-weight">(Vendor)</span></h4>
                    <h5>Holland Park West</h5>
                    <p>Dear Ross
                        With regards to rating your agency, firstly you were very professional and never at any time did you force your ideas onto me. Also you listened to what I had to say not like other real estate agents who, because I was an older single lady, thought I couldn't think for myself. They made me feel very uncomfortable. Emma and the photographer were also very professional. I would recommend you and your staff to any person who was planning to sell. I am sure the new owners of this block will enjoy the area as much as I have.
                        Thank you very much!
                    </p>

                    <h4>Gary O <span class="normal-weight">(Vendor)</span></h4>
                    <h5>Eight Mile Plains</h5>
                    <p>Social
                        Ross was professional and available at all times for any queries we had. He has an extensive knowledge of the local area and he was very easy to communicate with.
                    </p>

                    <h4>Chris & Raelee H <span class="normal-weight">(Vendor)</span></h4>
                    <h5>Parkinson</h5>
                    <p>We were recommended to use Ross as our agent by my Father who could not fault him in his efforts and dealings with Ross in the past. We have found Ross easy to talk too and understand which made the whole selling process a pleasurable experience. Ross is very personable but also very professional in the way he conducts himself. Ross keeps you up to date with all developments of the sale and it has been a pleasure to deal with him.
                    </p>

                    <h4>Shaney Hooley <span class="normal-weight">(Buyer)</span></h4>
                    <h5>Mt Gravatt East</h5>
                    <p>Ross was very honest and open as he was representing the owners of the house we purchased. Ross was very professional and we would definitely contact him when we plan to sell.
                    </p>

                    <h4>Brad F <span class="normal-weight">(Vendor)</span></h4>
                    <h5>Eight Mile Plains</h5>
                    <p>Ross conducted himself in a very professional manner. His advice on market expectations was relatively accurate, helpful and through the negotiations he was quite supportive. Ross continued to market our property until he felt he had got the best possible price. We always felt he was working for our best interests.
                    </p>

                    <h4>Rachel D <span class="normal-weight">(Vendor)</span></h4>
                    <h5>Drewvale</h5>
                    <p>Ross has sold three houses for me now, as always it was a seamless experience. Ross is personable, knowledgeable, and really looks after you as a seller. I have every confidence in Ross and his Team, and wouldn't hesitate to use them again.
                    </p>

                    <h4>Mladen G <span class="normal-weight">(Vendor)</span></h4>
                    <h5>Runcorn</h5>
                    <p>Recommended Ross Bischoff
                        Ross and his team are very professional and have a genuine knowledge of the local market. As a seller, I appreciated his quick, effective and no pressure sales approach. Ross kept all of his promises, maintained clear and open communication throughout the entire process, and transformed what is usually a very stressful experience into enjoyable one.
                    </p>

                    <h4>Jenny S <span class="normal-weight">(Vendor)</span></h4>
                    <h5>Eight Mile Plains</h5>
                    <p>My experience with Ross was memorable throughout the whole sale. He and his staff were well organized and made things easy for me by being professional with the Open House days. Ross knew the market quite well, and discussed everything in full with me, and when my property did sell, the sale price exceeded my expectations. I was very happy with my dealings with Ross, and would use him again
                    </p>

                    <h4>Merilyn A <span class="normal-weight">(Vendor)</span></h4>
                    <h5>Eight Mile Plains</h5>
                    <p>Ross is a well known agent in my area and his sales expertise lived up to all my expectations. Nothing was too much trouble for him and he kept me informed at all times.
                    </p>

                    <h4>Rosario P <span class="normal-weight">(Vendor)</span></h4>
                    <h5>Acacia Ridge</h5>
                    <p>Ross is an excellent and trustworthy agent!
                    </p>

                    <h4>Dianne E <span class="normal-weight">(Vendor)</span></h4>
                    <h5>Mt. Gravatt East</h5>
                    <p>Everything about Mum's house sale was perfect. It was a quicker sale than we thought and more than we thought Mum would get for it, so thanks to the team at Ross's Agency for their great work.
                    </p>

                    <h4>Kylie & Murray C. <span class="normal-weight">(Vendor)</span></h4>
                    <h5>Eight Mile Plains</h5>
                    <p>Dear Ross & Emma,
                        Thank you for all your efforts in the recent sale of our home of 13yrs. Twice now you have assisted us with the sale of family homes & twice you have persevered to yield us not just a good price, but a great price! Thanks again, Murray & Kylie C.
                    </p>

                    <h4>Pauline Armstrong <span class="normal-weight">(Vendor)</span></h4>
                    <h5>Runcorn</h5>
                    <p>Ross was extremely professional at the same time friendly and kind. He not only helped me every step of the way with the sale of my house but afterwards when I asked him for some help he was very gracious in providing all I needed to know. I just wish he was here in New Zealand to sell me a house . I trusted him completely and he had a way of always putting me at ease. Thank-you Ross for everything and to Emma who was very kind and helpful as well.
                    </p>

                    <h4>Sheryl T <span class="normal-weight">(Vendor)</span></h4>
                    <h5>Eight Mile Plains</h5>
                    <p>I chose to put Ross and his team on board to sell our home because I felt he was genuine and he listened to what we wanted. Ross and Emma addressed our concerns, were open and honest with us regarding some of the concerns that we may encounter with the selling of the property. The photos that we taken of the property were of a high standard which would of even enticed myself to look at the property. Emma and Ross were always available to take my calls and went beyond the call of duty on Christmas Eve when they were asked to prepare a contract on the property. Not once did they say they were not able to help. I enjoyed the banter and laughs we had and would recommend Ross and the Team for selling your home
                    </p>

                    <h4>Jim & Pattie C <span class="normal-weight">(Vendor)</span></h4>
                    <h5>Eight Mile Plains</h5>
                    <p>The presentation of our property was realistic. Ross kept us informed regarding offers and negotiations and the transfer was seamless.</p>
                <?php } ?>
            </div>
        </div>
    </div>
</section>