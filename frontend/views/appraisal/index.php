<?php
/* @var $this yii\web\View */
/* @var $properties common\models\Property[] */

use yii\helpers\Html;

$this->title = '';

?>


<section class="wrapper-sm">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Request an Appraisal</h1>
            </div>
        </div>
    </div>
</section>

<section class="wrapper-md bg-highlight">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <form role="form" method="post" action="/appraisal">
                    <div class="form-group">
                        <label for="example-contact-name">Name</label>
                        <input type="text" class="form-control" id="example-contact-name" placeholder="Your name" name="name" required>
                    </div>
                    <div class="form-group">
                        <label for="example-contact-email">Email address</label>
                        <input type="email" class="form-control" id="example-contact-email" placeholder="Your email" name="email" required>
                    </div>
                    <div class="form-group">
                        <label for="example-contact-name">Mobile Number</label>
                        <input type="tel" class="form-control" id="example-contact-phone" placeholder="Your mobile number" name="phone" required>
                    </div>
                     <div class="form-group">
                        <label for="example-contact-name">Property Address</label>
                        <input type="text" class="form-control" placeholder="Your property address" name="property_address" required>
                    </div>
                    <div class="form-group">
                        <label for="example-contact-name">Suburb</label>
                        <input type="text" class="form-control" id="example-contact-name" placeholder="Your suburb" name="suburb" required>
                    </div>
                    <div class="form-group">
                        <label for="example-contact-name">Post Code</label>
                        <input type="text" class="form-control" id="example-contact-name" placeholder="Your postcode" name="postcode" required>
                    </div>

                    <div class="form-group">
                        <label for="example-contact-message">Message</label>
                        <textarea id="example-contact-message" class="form-control" rows="5" name="message" minlength="10" required></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Send Message</button>
                </form>
            </div>
        </div>
    </div>
</section>