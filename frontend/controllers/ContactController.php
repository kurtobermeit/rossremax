<?php
namespace frontend\controllers;

use common\models\Property;
use Yii;
use frontend\components\Controller;
use common\models\Agent;


/**
 * Contact controller
 */
class ContactController extends Controller
{
    /**
     * Anonymous Actions allowed in this controller
     *
     * @var array
     */
    public $anonActions = ['property'];

    /**
     * @var bool
     */
    public $enableCsrfValidation = false;


    /**
     * Site Homepage
     *
     * @return string
     */
    public function actionProperty()
    {
        $id = intval($this->request->getQueryParam('id'));

        $fromEmail = $this->request->post('email');
        $fromName  = $this->request->post('name');
        $fromPhone = $this->request->post('phone');

        $toEmail   = Yii::$app->params['contactEmail'];
        $message   = $this->request->post('message');

        if ($_SERVER['REQUEST_METHOD'] == 'POST' && $id) {
            $property = Property::findOne(['id' => $id]);
            if ($property) {
                // Make sure it's owned by the filter
                $filterEmail = $this->params['filterAgentEmail'];
                if ($filterEmail) {
                    $agent = $property->getAgents()->where(['=', 'email', $filterEmail])
                        ->limit(1)
                        ->one();

                    if ($agent) {

                        // Send email
                        Yii::$app->mailer->compose([
                            'html' => 'contact/html',
                            'text' => 'contact/text',
                        ], [
                            'fromName'  => $fromName,
                            'fromEmail' => $fromEmail,
                            'fromPhone' => $fromPhone,
                            'msg'       => $message,
                            'property'  => $property,
                        ])
                            ->setFrom($fromEmail)
                            ->setTo($toEmail)
                            ->setSubject('Contact from ' . $fromName)
                            ->send();

                        $this->notifySuccess('Thanks ' . $fromName .'! Your message has been sent. We\'ll get back to you soon.');

                        return $this->redirect($property->getUrl());
                    }
                }
            }
        }

        return $this->redirect('/');
    }
}
