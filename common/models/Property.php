<?php

/**
 * Class common\models\Property
 *
 * @property-read int     $id
 * @property      int     $createdTime
 * @property      int     $updatedTime
 * @property      string  $type
 * @property      int     $renetId
 * @property      string  $address
 * @property      string  $authority
 * @property      bool    $isUnderOffer
 * @property      string  $category
 * @property      int     $auctionTime
 * @property      string  $displayPrice
 * @property      int     $bedrooms
 * @property      int     $bathrooms
 * @property      int     $garages
 * @property      bool    $isSold
 * @property      string  $landArea
 * @property      int     $soldTime
 * @property      string  $inspectionTimes
 * @property      string  $headline
 * @property      string  $description
 * @property      string  $listingAgents
 *
 */

namespace common\models;

use common\components\ActiveRecord;
use common\components\TypecastBehavior;
use yii\behaviors\TimestampBehavior;

class Property extends ActiveRecord
{

    /**
     * The Mysql Table Name
     *
     * @access public
     * @return string
     */
    public static function tableName()
    {
        return 'Property';
    }


    /**
     * Typecast Behavior
     *
     * @access public
     * @return array
     */
    public function behaviors()
    {
        $behaviours = [
            'timestampBehavior' => [
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdTime',
                'updatedAtAttribute' => 'updatedTime',
            ],
            'typecastBehavior' => [
                'class'  => TypecastBehavior::className(),
                'intFields' => [
                    'id',
                    'renetId',
                    'createdTime',
                    'updatedTime',
                    'auctionTime',
                    'soldTime',
                    'bedrooms',
                    'bathrooms',
                    'garages',
                ],
                'boolFields' => [
                    'isUnderOffer',
                    'isSold',
                ],
                'jsonFields' => [
                    'address',
                    'inspectionTimes',
                    'listingAgents',
                ],
            ],
        ];
        return array_merge(parent::behaviors(), $behaviours);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotos()
    {
        return $this->hasMany(Photo::className(), ['propertyId' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgents()
    {
        return $this->hasMany(Agent::className(), ['id' => 'agentId'])
            ->viaTable(PropertyHasAgent::tableName(), ['propertyId' => 'id']);
    }


    /**
     * @return string
     */
    public function getStreetAddress()
    {
        if ($this->address) {
            $parts = [];

            if (isset($this->address['streetNumber']) && $this->address['streetNumber']) {
                $parts['streetNumber'] = $this->address['streetNumber'];
            }

            if (isset($this->address['street']) && $this->address['street']) {
                $parts['street'] = $this->address['street'];
            }

            return implode(' ', $parts);
        }

        return '';
    }


    /**
     * @return string
     */
    public function getSuburbAddress()
    {
        if ($this->address) {
            $parts = [];

            if (isset($this->address['suburb']) && $this->address['suburb']) {
                $parts['suburb'] = ucwords(strtolower($this->address['suburb'])) . ',';
            }

            if (isset($this->address['state']) && $this->address['state']) {
                $parts['state'] = $this->address['state'] . ',';
            }

            if (isset($this->address['postcode']) && $this->address['postcode']) {
                $parts['postcode'] = $this->address['postcode'];
            }

            return implode(' ', $parts);
        }

        return '';
    }


    /**
     * @return string
     */
    public function getFullAddress()
    {
        if ($this->address) {
            $parts = [];

            if (isset($this->address['streetNumber']) && $this->address['streetNumber']) {
                $parts['streetNumber'] = $this->address['streetNumber'];
            }

            if (isset($this->address['street']) && $this->address['street']) {
                $parts['street'] = $this->address['street'] . ',';
            }

            if (isset($this->address['suburb']) && $this->address['suburb']) {
                $parts['suburb'] = ucwords(strtolower($this->address['suburb'])) . ',';
            }

            if (isset($this->address['state']) && $this->address['state']) {
                $parts['state'] = $this->address['state'] . ',';
            }

            if (isset($this->address['postcode']) && $this->address['postcode']) {
                $parts['postcode'] = $this->address['postcode'];
            }

            return implode(' ', $parts);
        }

        return '';
    }


    /**
     * @return string
     */
    public function getUrl()
    {
        $addressSlug = '';

        if ($this->address) {
            $parts = [];

            if (isset($this->address['streetNumber']) && $this->address['streetNumber']) {
                $parts['streetNumber'] = $this->address['streetNumber'];
            }

            if (isset($this->address['street']) && $this->address['street']) {
                $parts['street'] = $this->address['street'];
            }

            if (isset($this->address['suburb']) && $this->address['suburb']) {
                $parts['suburb'] = $this->address['suburb'];
            }

            $addressSlug = str_replace(' ', '-', str_replace('/', '', implode('-', $parts)));
        }

        return '/property/' . $this->id . '/' . strtolower($addressSlug);
    }

}
