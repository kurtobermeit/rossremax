<?php

/**
 * Class common\models\Property
 *
 * @property-read int     $id
 * @property      int     $createdTime
 * @property      int     $updatedTime
 * @property      string  $name
 * @property      string  $email
 * @property      string  $phone
 *
 */

namespace common\models;

use common\components\ActiveRecord;
use common\components\TypecastBehavior;
use yii\behaviors\TimestampBehavior;

class Agent extends ActiveRecord
{

    /**
     * The Mysql Table Name
     *
     * @access public
     * @return string
     */
    public static function tableName()
    {
        return 'Agent';
    }


    /**
     * Typecast Behavior
     *
     * @access public
     * @return array
     */
    public function behaviors()
    {
        $behaviours = [
            'timestampBehavior' => [
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdTime',
                'updatedAtAttribute' => 'updatedTime',
            ],
            'typecastBehavior' => [
                'class'  => TypecastBehavior::className(),
                'intFields' => [
                    'id',
                    'createdTime',
                    'updatedTime',
                ],
            ],
        ];
        return array_merge(parent::behaviors(), $behaviours);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperties() {
        return $this->hasMany(Property::className(), ['id' => 'propertyId'])
            ->viaTable(PropertyHasAgent::tableName(), ['agentId' => 'id']);
    }


    /**
     * @param  string  $orderBy
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getUnsoldProperties($orderBy = 'createdTime')
    {
        return $this->getProperties()
            ->where(['=', 'isSold', 0])
            ->orderBy($orderBy)
            ->all();
    }

    /**
     * @param  string  $orderBy
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getRecentSoldProperties($orderBy = 'createdTime')
    {
        return $this->getProperties()
            ->where(['=', 'isSold', 1])
            ->orderBy($orderBy)
            ->all();
    }
}
