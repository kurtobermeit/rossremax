<?php

/**
 * Class common\models\Property
 *
 * @property int     $propertyId
 * @property int     $agentId
 *
 */

namespace common\models;

use common\components\ActiveRecord;
use common\components\TypecastBehavior;
use yii\behaviors\TimestampBehavior;

class PropertyHasAgent extends ActiveRecord
{

    /**
     * The Mysql Table Name
     *
     * @access public
     * @return string
     */
    public static function tableName()
    {
        return 'PropertyHasAgent';
    }


    /**
     * Typecast Behavior
     *
     * @access public
     * @return array
     */
    public function behaviors()
    {
        $behaviours = [
            'timestampBehavior' => [
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdTime',
                'updatedAtAttribute' => 'updatedTime',
            ],
            'typecastBehavior' => [
                'class'  => TypecastBehavior::className(),
                'intFields' => [
                    'propertyId',
                    'agentId',
                ],
            ],
        ];
        return array_merge(parent::behaviors(), $behaviours);
    }
}
