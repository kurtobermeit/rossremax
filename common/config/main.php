<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'db' => [
            'class'             => 'yii\db\Connection',
            'dsn'               => 'mysql:host=127.0.0.1;dbname=emmaremax',
            'username'          => 'emmaremax',
            'password'          => '5fWqydaGee2yOMvs',
            'charset'           => 'utf8',
            'enableSchemaCache' => true,
        ],
        'cache' => [
            'class'     => 'yii\caching\ApcCache',
            'keyPrefix' => 'EDMREMAX_',
            'useApcu'   => true,
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            //'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class'  => 'sentry\SentryTarget',
                    'levels' => ['error', 'warning'],
                    'dsn'    => 'https://8cf8a06ce9624588a8c592d40290b317:b6369fc22b2a467baec71647eebcb9e1@app.getsentry.com/53237',
                ],
            ],
        ],
    ],
];
