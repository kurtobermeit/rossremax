<?php
return [
    'siteName'          => 'Ross Bischoff',

    'ownerName'         => 'Ross Bischoff',
    'color'             => '#0066B2',

    // This is the only Agent's properties that will show
    'filterAgentEmail' => 'rbischoff@remax.com.au',

    'teamEmails' => ['emma@remax.com.au', 'ross@remax.com.au'],

    // This is the from email address your emails with appear to come from
    'fromEmail' => 'ross@remax.com.au',

    'renetImportFolder' => '/var/www/emmaremax/import',

    // Change this to something, anything you want
    'secret' => 'oeohjgkjdfghe924wgkjgd239ed',

    // This is what will be displayed on the home page
    'contactEmail' => 'ross@remax.com.au',
    //'contactEmail' => 'rbischoff@remax.com.au',

    'cacheKeys' => [

    ],
];
